
from django.contrib import admin
from django.urls import path

from api.views import DriverView, DriversListCreate, VehiclesListCreate, VehicleView, SetDriver

urlpatterns = [
    path('admin/', admin.site.urls),
    path("drivers/driver/", DriversListCreate.as_view()),
    path("drivers/driver/?created_at__gte=10-11-2021", DriversListCreate.as_view()),
    path("drivers/driver/?created_at__lte=16-11-2021", DriversListCreate.as_view()),
    path("drivers/driver/<driver_id>/", DriverView.as_view()),
    path("vehicles/vehicle/", VehiclesListCreate.as_view()),
    path("vehicles/vehicle/?with_drivers=yes", VehiclesListCreate.as_view()),
    path("vehicles/vehicle/?with_drivers=no", VehiclesListCreate.as_view()),
    path("vehicles/vehicle/<vehicle_id>", VehicleView.as_view()),
    path("vehicles/set_driver/<vehicle_id>/", SetDriver.as_view())

]
