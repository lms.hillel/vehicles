**To install the application on your local computer, you need Python 3.9 or higher.**

- Download the application to your computer by selecting "download source code" and the required archive format for your OS. 
Unpack the archive into a folder of your choice.
(If you are using git, you can clone the project using the command: `git clone git@gitlab.com:lms.hillel/vehicles.git` )

- In the console go to the project folder, which contains the file manage.py

- Create a virtual environment for the project:  

  `python -m venv venv`

- Activate virtual environment:  

  `venv\Scripts\activate.bat` (for Windows)  

  `source venv/bin/activate` (for Linux)  

  Make sure the viral environment is active: the console displays (venv)

- Install required packages:  

  `pip install -r requirements.txt`

- Create database and run the project:  

  `python manage.py migrate`  

  `python manage.py runserver`

If you see a link to your local server - the application is running! Use a browser to navigate to endpoints
