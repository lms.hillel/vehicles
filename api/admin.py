from django.contrib import admin

from api.models import Driver, Vehicle

admin.site.register([Driver, Vehicle])
