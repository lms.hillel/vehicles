from django.core.validators import RegexValidator
from django.db import models
from django.db.models import SET_NULL


class BaseModel(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(null=True, auto_now_add=True)
    updated_at = models.DateTimeField(null=True, auto_now=True)


class Driver(BaseModel):
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Vehicle(BaseModel):
    driver_id = models.ForeignKey(to=Driver, related_name="vehicles",
                                  on_delete=SET_NULL, null=True, blank=True)
    make = models.CharField(max_length=60)
    model = models.CharField(max_length=60)
    plate_number = models.CharField(max_length=10,
                                    validators=[RegexValidator('^[A-ZА-Я]{2}\s\d{4}\s[A-ZА-Я]{2}$')]) # noqa

    def __str__(self):
        return f"{self.make} {self.model}"
