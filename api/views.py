from datetime import datetime

from django.shortcuts import get_object_or_404
from rest_framework.exceptions import ValidationError
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, UpdateAPIView
from api.models import Driver, Vehicle
from api.serializers import DriverSerializer, VehicleSerializer, VehicleSetDriverSerializer


class DriversListCreate(ListCreateAPIView):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        param_gte = self.request.query_params.get('created_at__gte')
        param_lte = self.request.query_params.get('created_at__lte')
        date_format = "%d-%m-%Y"
        if param_gte:
            param_gte = datetime.strptime(param_gte, date_format)
            queryset = queryset.filter(created_at__gte=param_gte)
        if param_lte:
            param_lte = datetime.strptime(param_lte, date_format)
            queryset = queryset.filter(created_at__lte=param_lte)

        return queryset


class DriverView(RetrieveUpdateDestroyAPIView):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer

    def get_object(self):
        driver_id = self.kwargs.get('driver_id')
        if driver_id.isdigit():
            obj = get_object_or_404(self.queryset, id=driver_id)
        else:
            raise ValidationError('ERROR: driver_id must be a number')
        return obj


class VehiclesListCreate(ListCreateAPIView):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        with_drivers = self.request.query_params.get('with_drivers')
        if with_drivers == 'yes':
            queryset = queryset.filter(driver_id__isnull=False)
        if with_drivers == 'no':
            queryset = queryset.filter(driver_id__isnull=True)

        return queryset


class SetDriver(UpdateAPIView):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSetDriverSerializer

    def get_object(self):
        vehicle_id = self.kwargs.get('vehicle_id')
        if vehicle_id.isdigit():
            obj = get_object_or_404(self.queryset, id=vehicle_id)
        else:
            raise ValidationError('ERROR: vehicle_id must be a number')
        return obj


class VehicleView(SetDriver, RetrieveUpdateDestroyAPIView):
    serializer_class = VehicleSerializer
