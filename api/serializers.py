from rest_framework.serializers import ModelSerializer

from api.models import Driver, Vehicle


class DriverSerializer(ModelSerializer):

    class Meta:
        model = Driver
        fields = '__all__'


class VehicleSerializer(ModelSerializer):

    class Meta:
        model = Vehicle
        fields = '__all__'


class VehicleSetDriverSerializer(ModelSerializer):

    class Meta:
        model = Vehicle
        fields = ['driver_id']
